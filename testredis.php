<?php

//-----------------------装饰器例子------------------------
$show = function ($name) {
    return $name;
};

function A($func)
{
    return function ($params) use ($func) {
        $params = "复仇者联盟 " . $params;
        return call_user_func($func, $params);
    };
}


function B($func)
{
    return function ($params) use ($func) {
        $params = "超人归来 " . $params;
        return call_user_func($func, $params);
    };
}

echo A($show)("frans") . PHP_EOL;
echo B($show)("frank") . PHP_EOL;

//----------------------装饰器例子2------------------------------


class Me
{
    function show($name)
    {
        return $name;
    }
}


$refClass = new ReflectionClass("Me");
$redMenthod = $refClass->getMethod("show");
echo $redMenthod->invoke(new Me(), "frans") . PHP_EOL;
