# phpfans

#### 介绍
采用annotation和di基于swoole实现httpserver 的 php框架 

swoole httpserver fast-route  di  annotations  mysql pool  redis pool redis lock 

#### 软件架构
仅仅实现底层功能。应用开发还需自行构建！


#### 软件安装  
下载
git clone 地址 

复制文件
cp .env.example .env 

composer 扩展安装 

composer update 

启动项目：

php bin/frans start