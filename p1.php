<?php


use Swoole\Process;

echo '当前进程ID' . posix_getppid() . PHP_EOL;
cli_set_process_title('frans master');


$child1 = new Process(function (Process $process) {

    cli_set_process_title("frans child 1");

    //执行外部文件就 自动退出（下面没有sleep的情况）
    $process->exec("/usr/bin/env", ['php', './test.php']);

}, true, 0, true);
$child1->start();


//主进读取子进程数据 并输出
$data = $child1->read();
var_dump($data);


Process::signal(SIGCHLD, function ($sig) {
    while ($ret = Process::wait(false)) {
        var_dump($ret);
    }
});





