<?php

use Core\server\HttpServer;
use Swoole\Process;

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/app/config/define.php';

if ($argc == 2) {
    $cmd = $argv[1];

    if ($cmd == 'start') {
        $server = new HttpServer();
        $server->run();
    } elseif ($cmd == 'reload') {

        $master_pid = intval(file_get_contents(ROOT_PATH . '/phpfans.pid'));

        if ($master_pid && trim($master_pid) != 0) {
            Process::kill($master_pid, SIGUSR1);
        }

    } elseif ($cmd == 'stop') {
        $master_pid = intval(file_get_contents(ROOT_PATH . '/phpfans.pid'));

        if ($master_pid && trim($master_pid) != 0) {
            Process::kill($master_pid);
        }
    } else {
        echo '命令无效，请使用 start stop reload restart 等关键字';
    }
}
echo 'success！' . PHP_EOL;