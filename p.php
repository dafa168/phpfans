<?php


use Swoole\Coroutine\MySQL;
use Swoole\Process;

echo '当前进程ID' . posix_getppid() . PHP_EOL;
cli_set_process_title('frans master');


$child1 = new Process(function (Process $process) {

    cli_set_process_title("frans child 1");

    $mysql = new Mysql();
    $mysql->connect([
        'host' => '192.168.101.241',
        'user' => 'root',
        'password' => '123456',
        'database' => 'hx_one_by_one'
    ]);

    $offset = 0;
    while (true) {
        $sql = "select * from `test` where `status` = 1 limit {$offset},1";
        $rs = $mysql->query($sql);
        if ($rs && count($rs) == 1) {
            //var_dump($rs);
            $process->push($rs[0]['name']);
            $offset++;
        }

        sleep(3);
    }
}, false, 1, true);

$child1->useQueue(2);
$child1->start();


$child2 = new Process(function (Process $process) {

    while (true) {
        usleep(0.5 * 1000 * 1000);
        $name = $process->pop();
        echo '进程2， 取得用户：' . $name . ', 邮件发送成功！ ' . PHP_EOL;
    }

});

$child2->useQueue(2);
$child2->start();


$child3 = new Process(function (Process $process) {

    while (true) {
        usleep(0.5 * 1000 * 1000);
        $name = $process->pop();
        echo '进程3， 取得用户：' . $name . ', 邮件发送成功！ ' . PHP_EOL;
    }

});

$child3->useQueue(2);
$child3->start();

while (true) {
    sleep(1);
}

Process::signal(SIGCHLD, function ($sig) {
    while ($ret = Process::wait(false)) {
        var_dump($ret);
    }
});





