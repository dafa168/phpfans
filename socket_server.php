<?php

$serv = new \Swoole\Server("0.0.0.0", 9501);

$serv->on('receive', function (swoole_server $serv, $fd, $from_id, $data) {
    $serv->send($fd, "hello swoole");
    $serv->close($fd);
});

//启动服务器
$serv->start();
