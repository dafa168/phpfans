<?php

/**
 * Redis连接池配置
 */
return [

    //默认配置
    'default' => [
        'min' => 5,  //最小链接数
        'max' => 10, //最大链接数
        'idle_time' => 20 //空闲时间
    ]
];