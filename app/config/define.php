<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/6 13:30
 * +------------------------------
 */


define('ROOT_PATH', dirname(dirname(__DIR__)));

/**
 * 全局配置
 */
$GLOBAL_CONFIGS = [
    'db' => require_once __DIR__ . '/db.php',
    'dbpool' => require_once __DIR__ . '/dbpool.php',
    'redis' => require_once __DIR__ . '/redis.php',
    'redispool' => require_once __DIR__ . '/redispool.php',

];