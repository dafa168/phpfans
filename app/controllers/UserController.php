<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/6 13:18
 * +------------------------------
 */

namespace App\controllers;

use App\models\Users;
use Core\annotations\Bean;
use Core\annotations\DB;
use Core\annotations\Redis;
use Core\annotations\RequestMapping;
use Core\annotations\Value;
use Core\http\Request;
use Core\http\Response;
use Core\init\MyDB;

/**
 * @Bean(name="user")
 */
class UserController
{
    /**
     * @Value(name="version")
     */
    public $version;

    /**
     * @DB(source="default")
     * @var MyDB
     */
    private $db;


    /**
     * @DB(source="db2")
     * @var MyDB
     */
    private $dba;

    /**
     * @RequestMapping(value="/user")
     */
    public function test()
    {

        $rs = Users::query()->first()->toArray();
        var_dump($rs);

        return 'user';
    }

    /**
     * @RequestMapping(value="/zxc")
     */
    public function zxc()
    {

        $rs = $this->dba->table('wp_users')->first();
        var_dump($rs);

        return 'user';
    }

    /**
     * @RequestMapping(value="/trans")
     */
    public function trans()
    {
        $tx = $this->db->begin();
        $this->db->table('wp_users')->insert(['user' => 11]);

        $tx->commit();
        //$tx->rollback() // 提交或者回滚;

        return 'ok';
    }

    /**
     * @RequestMapping(value="/user/{uid:\d+}")
     */
    public function user(Request $request, Response $response, int $uid)
    {
        var_dump($request->getQueryParams());
        echo __METHOD__ . PHP_EOL;
        return 'frans ' . $uid;
    }

    /**
     * TODO: 这里的 Redis 是为了把数据库查询出来的数据直接缓存到redis，
     * TODO: 下次查询则走redis获取数据
     * @Redis(prefix="string", key="users")
     * @RequestMapping(value="/redis")
     */
    public function testredis(Request $request)
    {

    }

    /**
     * TODO: 这里的 Redis 是为了把数据库查询出来的数据直接缓存到redis，
     * TODO: 下次查询则走redis获取数据
     * @Redis(prefix="users", key="#123", expire="96400", incr="xxxx")
     * @RequestMapping(value="/redis")
     */
    public function testredis2(Request $request)
    {

    }
}