<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/11 13:21
 * +------------------------------
 */

namespace App\models;


use Core\lib\DBModel;

class Users extends DBModel
{
    protected $table = 'wp_users';
    protected $primaryKey = 'ID';

    protected $connection = 'db2' ;

}