<?php


namespace App\models;


use Core\lib\DBModel;

/**
 * 商品模型
 * Class Products
 * @package App\models
 */
class Products extends DBModel
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    public $timestamps = false;

}