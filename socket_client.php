<?php

/**
 * php socket client 实现
 */
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_connect($socket, "127.0.0.1", 9501);
socket_write($socket, "hello server");
$out = socket_read($socket, 1024);
echo $out;
socket_close($socket);


/**
 * swoole client 实现
 */
$client = new swoole_client(SWOOLE_SOCK_TCP);
$client->connect("127.0.0.1", 9501);
$client->send("hello client");
echo $client->recv();
$client->close();
