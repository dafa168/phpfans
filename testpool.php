<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/12 9:32
 * +------------------------------
 */

require __DIR__ . '/vendor/autoload.php';

go(function () {

    echo 'coroutine ....' . PHP_EOL;

    $pool = new \Core\pool\PDOPool();
    $pool->initPool();

    for ($i = 0; $i < 11; $i++) {
        go(function () use ($pool, $i) {

            $conn = $pool->getConnection();

            $state = $conn->db->query("select {$i}");
            $state->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $state->fetchAll();
            var_dump($rows);

        });
    }

    echo "当前总连接数：" . $pool->getCounts() . PHP_EOL;

    while (true) {
        \Swoole\Coroutine::sleep(2);
    }

});

echo __FILE__ . PHP_EOL;