<?php


namespace Core\lib;

use Core\annotations\Bean;
use Core\BeanFactory;
use Core\init\PHPRedisPool;

/**
 * @method static get(string $key)
 * @method static set(string $key, string $value)
 * @method static setex($key, $ttl, $value)
 *
 *
 * @method static hSet($key, $hashKey, $value)
 * @method static hSetNx($key, $hashKey, $value)
 * @method static hLen($key)
 * @method static hDel($key, $hashKey1, ...$otherHashKeys)
 * @method static hKeys($key)
 * @method static hVals($key)
 * @method static hGetAll($key)
 * @method static hExists($key, $hashKey)
 * @method static hIncrBy($key, $hashKey, $value)
 * @method static hIncrByFloat($key, $field, $increment)
 * @method static hMSet($key, $hashKeys)
 * @method static hMGet($key, $hashKeys)
 * @method static hScan($key, &$iterator, $pattern = null, $count = 0)
 * @method static hStrLen(string $key, string $field)
 *
 *
 *
 * @method static zAdd($key, $options, $score1, $value1, $score2 = null, $value2 = null, $scoreN = null, $valueN = null)
 * @method static zRange($key, $start, $end, $withscores = null)
 * @method static zRem($key, $member1, ...$otherMembers)
 * @method static zDelete($key, $member1, ...$otherMembers)
 * @method static zRevRange($key, $start, $end, $withscore = null)
 * @method static zRangeByScore($key, $start, $end, array $options = array())
 * @method static zRevRangeByScore($key, $start, $end, array $options = array())
 * @method static zRangeByLex($key, $min, $max, $offset = null, $limit = null)
 * @method static zRevRangeByLex($key, $min, $max, $offset = null, $limit = null)
 * @method static zCount($key, $start, $end)
 * @method static zRemRangeByScore($key, $start, $end)
 * @method static zDeleteRangeByScore($key, $start, $end)
 * @method static zRemRangeByRank($key, $start, $end)
 * @method static zDeleteRangeByRank($key, $start, $end)
 * @method static zCard($key)
 * @method static zSize($key)
 * @method static zScore($key, $member)
 * @method static zRank($key, $member)
 * @method static zRevRank($key, $member)
 * @method static zIncrBy($key, $value, $member)
 * @method static zUnionStore($output, $zSetKeys, array $weights = null, $aggregateFunction = 'SUM')
 * @method static zUnion($Output, $ZSetKeys, array $Weights = null, $aggregateFunction = 'SUM')
 * @method static zInterStore($output, $zSetKeys, array $weights = null, $aggregateFunction = 'SUM')
 * @method static zInter($Output, $ZSetKeys, array $Weights = null, $aggregateFunction = 'SUM')
 * @method static zScan($key, &$iterator, $pattern = null, $count = 0)
 *
 *
 *
 * @method static eval($script, $args = array(), $numKeys = 0)
 *
 *
 *
 *
 *
 * @Bean()
 */
class RedisHelper
{

    public static function __callStatic($name, $arguments)
    {
        /**
         * @var $pool PHPRedisPool
         */
        $pool = BeanFactory::getBean(PHPRedisPool::class);
        $redis_obj = $pool->getConnection();

        try {

            if (!$redis_obj) return false;
            $redis = $redis_obj->redis;
            return $redis->$name(... $arguments);

        } catch (\Throwable $e) {

            echo __METHOD__ . '::' . $e->getMessage() . '-File :' . $e->getFile() . '-Line:' . $e->getLine();

            return null;

        } finally {
            if ($redis_obj) {
                $pool->close($redis_obj);
            }
        }
    }

}