<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/11 13:22
 * +------------------------------
 */

namespace Core\lib;


use Core\BeanFactory;
use Core\init\MyDB;
use Illuminate\Database\Eloquent\Model;

/**
 * 基类
 * Class DBModel
 * @package Core\lib
 */
class DBModel extends Model
{

    /**
     * 重写 __call
     * @param string $method
     * @param array $parameters
     * @return mixed|void
     */
    public function __call($method, $parameters)
    {
        return $this->invoke(function () use ($method, $parameters) {
            return parent::__call($method, $parameters);
        });
    }

    /**
     * 重写 save 方法
     * @param array $options
     * @return bool|void
     */
    public function save(array $options = [])
    {
        return $this->invoke(function () use ($options) {
            return parent::save($options);
        });
    }

    /**
     * 取得全部
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public static function all($columns = ['*'])
    {
        return self::invokeStatic(function () use ($columns) {
            return parent::all($columns);
        });
    }

    /**
     * 公共执行方法
     * @param callable $func
     * @return mixed
     */
    private function invoke(callable $func)
    {
        $mydb = clone BeanFactory::getBean(MyDB::class);
        $pdo_obj = $mydb->generateConnection();

        try {
            return $func();
        } catch (\Throwable $e) {
            echo __METHOD__ . '::' . $e->getMessage() . '-File :' . $e->getFile() . '-Line:' . $e->getLine();
        } finally {
            $mydb->releaseConnection($pdo_obj);
        }
    }

    private function invokeStatic(callable $func)
    {
        $mydb = clone BeanFactory::getBean(MyDB::class);
        $pdo_obj = $mydb->generateConnection();

        try {
            return $func();
        } catch (\Throwable $e) {
            echo __METHOD__ . '::' . $e->getMessage() . '-File :' . $e->getFile() . '-Line:' . $e->getLine();
        } finally {
            $mydb->releaseConnection($pdo_obj);
        }
    }

    /**
     * 重写call方法
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    /*public function __call($method, $parameters)
    {
//        if (in_array($method, ['increment', 'decrement'])) {
//            return $this->$method(...$parameters);
//        }
//        return $this->forwardCallTo($this->newQuery(), $method, $parameters);


        //@var $mydb MyDB
        $mydb = clone BeanFactory::getBean(MyDB::class);
        $pdo_obj = $mydb->generateConnection();

        try {
            $result = parent::__callStatic($method, $parameters);
            return $result;
        } catch (\Throwable $e) {
            echo __METHOD__ . '::' . $e->getMessage() . '-File :' . $e->getFile() . '-Line:' . $e->getLine();
        } finally {
            $mydb->releaseConnection($pdo_obj);
        }
    }*/


}