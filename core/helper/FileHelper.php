<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/10 10:04
 * +------------------------------
 */

namespace Core\helper;

class FileHelper
{
    /**
     * 递归取得文件MD5值
     * @param $dir
     * @param $ignore
     * @return string
     */
    public static function getFileMD5($dir, $ignore)
    {
        $files = glob($dir);
        $md5Val = [];

        foreach ($files as $file) {

            $fileinfo = is_file($file) ? pathinfo($file) : [];

            //如果是文件夹，则递归，主要要加上/* ，否则获取不到内容
            if (is_dir($file) && strpos($file, $ignore) === false) {
                $md5Val[] = self::getFileMD5($file . '/*', $ignore);
            } elseif (isset($fileinfo['extension']) && $fileinfo['extension'] == 'php') {
                $md5Val[] = md5_file($file);
            }
        }

        return md5(implode('', $md5Val));
    }
}