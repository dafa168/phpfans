<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/6 21:00
 * +------------------------------
 */

namespace Core\init;

use Core\annotations\Bean;

/**
 * @Bean(name="router")
 */
class RouterCollect
{

    public $routes = [];

    public function addRouter($method, $uri, $handlers)
    {
        $this->routes[] = ['method' => $method, 'uri' => $uri, 'handler' => $handlers];
    }


    /**
     * 设置 dispatcher
     */
    public function getDispatcher()
    {
        $dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r) {
//    $r->addRoute('GET', '/users', 'get_all_users_handler');
// {id} must be a number (\d+)
//    $r->addRoute('GET', '/user/{id:\d+}', 'get_user_handler');
// The /{title} suffix is optional
//    $r->addRoute('GET', '/articles/{id:\d+}[/{title}]', 'get_article_handler');

            foreach ($this->routes as $route) {
                $r->addRoute($route['method'], $route['uri'], $route['handler']);
            }
        });


        return $dispatcher;
    }

}