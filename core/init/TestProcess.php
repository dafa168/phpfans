<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/9 14:01
 * +------------------------------
 */

namespace Core\init;


use Core\helper\FileHelper;
use Swoole\Process;

class TestProcess
{
    private $md5file;

    public function run()
    {
        return new  Process(function () {
            while (true) {
//                echo 'frans phper' . PHP_EOL;
                sleep(2);

                //获取文件列表 并计算MD5 值
                $md5_value = FileHelper::getFileMD5(ROOT_PATH . '/app/*', '/app/config');

                if ($this->md5file == '') {
                    $this->md5file = $md5_value;
                    continue;
                }

                //对比文件md5值
                if (strcmp($this->md5file, $md5_value) !== 0) {
                    echo 'PHP文件有改动, reloading .....' . PHP_EOL;
                    $this->md5file = $md5_value;

                    $master_pid = intval(file_get_contents(ROOT_PATH . '/tmp/phpfans.pid'));
                    if ($master_pid && trim($master_pid) != 0) {
                        Process::kill($master_pid, SIGUSR1);
                        echo 'reload success' . PHP_EOL;
                    }
                }
            }
        });
    }

    public function runbak()
    {
        return new  Process(function () {
            while (true) {
                echo 'frans phper' . PHP_EOL;
                sleep(3);
            }
        });
    }

}