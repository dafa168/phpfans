<?php


namespace Core\init;


use Core\annotations\Bean;
use Core\lib\RedisPool;

/**
 * Redis连接池实现
 * Class PHPRedisPool
 * @package Core\init
 * @Bean()
 */
class PHPRedisPool extends RedisPool
{
    public function __construct($min = 5, $max = 10, $idleTime = 10)
    {
        global $GLOBAL_CONFIGS;
        $poolConfig = $GLOBAL_CONFIGS['redispool']['default'];
        parent::__construct(($poolConfig['min'] ?? $min), ($poolConfig['max'] ?? $max), ($poolConfig['idle_time'] ?? $idleTime));
    }

    /**
     * 创建DB对象（抽象方法）
     * @return mixed
     */
    protected function newRedis()
    {
        global $GLOBAL_CONFIGS;

        $default = $GLOBAL_CONFIGS['redis']['default'];

        $redis = new \Redis();

        $redis->connect($default['host'], $default['port']);
        if (!empty($default['auth'])) {
            $redis->auth($default['auth']);
        }
        return $redis;
    }
}