<?php


namespace Core\init;

use Core\annotations\Bean;

/**
 * 装饰器收集类
 * Class DecoratorCollector
 * @Bean()
 */
class DecoratorCollector
{
    /**
     *
     * @var array
     */
    public $dSet = [];

    /**
     * 执行
     * @param \ReflectionMethod $method 方法名
     * @param null $instance 实例
     * @param array|null $inputParams 输入参数
     * @return mixed
     */
    public function exec(\ReflectionMethod $method, $instance, $inputParams)
    {
        //先获取KEY
        $key = (get_class($instance) . '::' . $method->getName());

        //判断KEY是否在dSet中
        if (isset($this->dSet[$key])) {
            //获取方法名
            $func = $this->dSet[$key];
            //执行方法
            return $func($method->getClosure($instance))($inputParams);
        }

        //不在dSet，则直接执行
        return $method->invokeArgs($instance, $inputParams);
    }

}