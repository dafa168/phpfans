<?php


namespace App\pool;


use Illuminate\Support\Facades\Config;

/**
 * Swoole 协程 Mysql
 * Class CoMysqlPool
 * @package App\pool
 */
class CoMysqlPool extends DBPool
{
    private $config;

    public function __construct($min = 5, $max = 10, $idleTime = 10)
    {
        parent::__construct($min, $max, $idleTime);

        //处理配置
        //$this->config = Config::get("db2");
    }

    /**
     * @return mixed
     */
    protected function newDB()
    {
        $mysql = new \Swoole\Coroutine\MySQL();
        return $mysql->connect([
            'host' => $this->config['host'] ?? '127.0.0.1',
            'user' => $this->config['user'],
            'password' => $this->config['password'],
            'port' => $this->config['port'] ?? 3306,
            'database' => $this->config['database'],
        ]);
    }
}