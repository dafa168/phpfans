<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/12 9:26
 * +------------------------------
 */

namespace Core\init;

use Core\annotations\Bean;
use Core\lib\DBPool;

/**
 * PDO连接池实现
 * Class PDOPool
 * @package Core\pool
 * @Bean()
 */
class PDOPool extends DBPool
{
    public function __construct(int $min = 5, int $max = 10, $idle_time = 10)
    {

        global $GLOBAL_CONFIGS;
        $poolConfig = $GLOBAL_CONFIGS['dbpool']['default'];
        parent::__construct(($poolConfig['min'] ?? $min), ($poolConfig['max'] ?? $max), ($poolConfig['idle_time'] ?? $idle_time));
    }

    /**
     *
     * @return mixed|\PDO
     */
    protected function newDB()
    {
        //新方法
        global $GLOBAL_CONFIGS;
        $default = $GLOBAL_CONFIGS['db']['default'];

        $driver = $default['driver'];
        $host = $default['host'];
        $port = $default['port'];
        $dbname = $default['database'];
        $dsn = "{$driver}:host={$host};port={$port};dbname={$dbname}";

        $pdo = new \PDO($dsn, ($default['username'] ?? ''), ($default['password'] ?? ''));
        return $pdo;
    }
}