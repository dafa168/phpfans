<?php

namespace Core\annotationhandlers;


use Core\annotations\DB;
use Core\BeanFactory;
use Core\init\MyDB;

return [
    /**
     * 属性注解
     */
    DB::class => function (\ReflectionProperty $prop, $instance, $self) {
        //$mydb_bean = BeanFactory::getBean(MyDB::class);
//        $bean->setDbSource($self->source);

        $mydb_bean = null;
        //如果不是 default；连接  则 重新设置一个新的 bean 进去
        if ($self->source !== 'default') {

            echo '第一次启动会默认走这里' . PHP_EOL;

            $bean_name = MyDB::class . '_' . $self->source;
            $mydb_bean = BeanFactory::getBean($bean_name);
            if (!$mydb_bean) {
                $mydb_bean = clone BeanFactory::getBean(MyDB::class);
                $mydb_bean->setDbSource($self->source);
                BeanFactory::setBean($bean_name, $mydb_bean);
            } //TODO:不懂这里为嘛也要clone
            else {
                $mydb_bean = clone $mydb_bean;
            }
        } else {
            $mydb_bean = clone BeanFactory::getBean(MyDB::class);
        }

        $prop->setAccessible(true);
        $prop->setValue($instance, $mydb_bean);
        return $instance;
    }

];


