<?php
/**
 * +------------------------------
 * Created by PhpStorm.
 * +------------------------------
 * User: xxx
 * +------------------------------
 * DateTime: 2019/12/6 13:45
 * +------------------------------
 */

namespace Core\annotationhandlers;

use Core\annotations\Bean;
use Core\annotations\Value;
use DI\Container;


return [

    /**
     * 类注解
     * TODO:这里bean的作用就是 把 实例对象设置到 容器中
     */
    Bean::class => function ($instance, $container, $self) {

        /**
         * @var Container $container
         * @var Object $instance
         */
        $vars = get_object_vars($self);
        if (isset($vars['name']) && "" != $vars['name']) {
            $beanName = $vars['name'];
        } else {
            $arrs = explode("\\", get_class($instance));
            $beanName = end($arrs);
        }
        $container->set($beanName, $instance);
    },


    /**
     * 属性注解
     */
    Value::class => function (\ReflectionProperty $prop, $instance, $self) {
        $env = parse_ini_file(ROOT_PATH . '/.env');

        //没有设置或为为空 则直接返回
        if (!isset($env[$self->name]) || $self->name == '') return $instance;

        //设置属性
        $prop->setValue($instance, $env[$self->name]);
        return $instance;
    }
];