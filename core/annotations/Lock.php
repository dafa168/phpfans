<?php


namespace Core\annotations;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Lock
 * @package Core\annotations
 * @Annotation
 * @Target({'METHOD'})
 */
class Lock
{
    public $prefix = '';
    public $key = '';
    public $retry = 3; // 重试次数
    public $expire = 10; // 过期时间 10秒

}