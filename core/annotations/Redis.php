<?php

namespace Core\annotations;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Redis annotations
 * @Annotation
 * @Target({"METHOD"})
 */
class Redis
{
    /**
     * 连接字符串
     * @var string
     */
    public $source = 'default';


    /**
     * key的前缀
     * @var string
     */
    public $prefix = '';

    /**
     * 键名
     * @var string
     */
    public $key = '';

    /**
     * redis 数据类型
     * @var string
     */
    public $type = 'string';

    /**
     * 过期时间
     * @var int
     */
    public $expire = -1;

    /**
     * 自增长字段名
     * @var string
     */
    public $incr = ''; //暂时只支持hash

    /**
     * 自减字段名
     * @var string
     */
    public $decr = '';


    public $score = ''; //sortedSet 专用
    public $member = ''; //sortedSet 专用


    /**
     * 是否协程化
     * @var bool
     */
    public $coroutine = false;

    /**
     * 支持执行 lua 脚本
     * @var string
     */
    public $script = '';

}

